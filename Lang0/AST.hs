
module Lang0.AST where

import Data.Fix

import Pretty

import Prelude hiding ((<>))

type AST name = Fix (AST_ name)

data AST_ name self
  = Var name                             -- a, b, foo
  | App self self                        -- f a, f(g)
  | Let [Decl name self] self            -- let x = f a b in add x x
  | Match self [Alt name self]           -- match foo with Cons x xs -> (+ x (sum xs))
  | If [IfAlt self]                      -- if | x == 0 -> 0 | x == 1 -> 1
  | Module self                          -- let list = module ...
  | Constant (Constant name self)
  deriving stock (Show, Functor)

data Constant name ast
  = Lam [Alt name ast]        -- \a -> + a 1
  | Object [Decl name ast]               -- { a = 1; b = a + 1 }
  | Tuple [ast]
  | List [ast]
  | Literal (Literal ast)
  deriving stock (Show, Functor)

data Literal ast
  = Integer String
  | Float   String
  | String  String [(ast, String)]
  deriving stock (Show, Functor)

data Decl name ast
  = Capture name                         -- capture a;
  | Func name [Pattern name ast] ast (Where name ast)          -- val a x = + x 1;
  | Bind (Pattern name ast) ast (Where name ast)         -- match A x <- + x 1;
  | Data (Data name)                     -- data List = | Cons head tail | Nil
  | Import [name] (Maybe (Pattern name ast))
  deriving stock (Show, Functor)

data Where name ast
  = Where [Decl name ast]
  deriving stock (Show, Functor)

data Data name
  = AddType name [Ctor name]
  deriving stock (Show)

data Ctor name
  = Ctor name [name]
  deriving stock (Show)

data Alt name ast
  = Alt [Pattern name ast] ast           -- Cons x xs -> xs
  deriving stock (Show, Functor)

data Pattern name ast
  = Hole name                            -- a
  | IsTuple [Pattern name ast]
  | IsCtor name [Pattern name ast]       -- Cons ...
  | IsLiteral (Literal ast)                    -- 1
  | IsObject [IsDecl name ast]           -- { a; b as c }
  | IsList [Pattern name ast]
  | As (Pattern name ast) name           -- b as c
  | View ast (Pattern name ast)          -- (sort -> b)
  | Strict (Pattern name ast)
  | Loop name
  deriving stock (Show, Functor)

data IsDecl name ast
  = IsDecl name (Pattern name ast)
  | IsCapture name
  deriving stock (Show, Functor)

data IfAlt ast
  = IfAlt [ast] ast
  deriving stock (Show, Functor)

instance Pretty n => Pretty (Pattern n Doc) where
  pp = \case
    Hole n -> pp n
    IsTuple xyz -> p "(" <> (fsep $ punctuate "," $ map pp xyz) <> p ")"
    IsCtor c fs -> p "(" <>  (pp c <+> fields fs) <> p ")"
    IsLiteral l -> pp l
    IsObject o -> p "{" <> (fsep $ punctuate "," $ map pp o) <> p "}"
    IsList o -> p "[" <> (fsep $ punctuate "," $ map pp o) <> p "]"
    As p n -> pp p <+> r "as" <+> pp n
    View f pat -> p "(" <> (f <+> p "->" <+> pp pat) <> p ")"
    Strict pat -> p "!" <> pp pat
    Loop n -> p "@" <> pp n

instance Pretty n => Pretty (IsDecl n Doc) where
  pp = \case
    IsDecl n pat -> pp n <+> p "=" <+> pp pat
    IsCapture n -> pp n

instance Pretty (IfAlt Doc) where
  pp (IfAlt conds b) =
    (block (map ((p "|" <+>) . pp) conds) <+> p "->")
      `indent` b

instance Pretty String where
  pp = text

indent  a b = hang a 2 b
andThen a b = hang a 0 b
block   az  = vcat (map pp az)
list    a b = indent a (fsep (map pp b))
chain   a   = fsep $ punctuate "." $ map pp a
fields  a   = fsep $ map pp a

p = tint (Light White)
r = tint (Light Blue)
g = tint (Light Green)
h = tint (Light Yellow)

instance Pretty Doc where
  pp = id

instance Pretty n => Pretty (AST n) where
  pp = cata \case
    Var n -> pp n
    App f x -> p "(" <> f <+> x <> p ")"
    Let decls b ->
      indent (r "let") (block decls) `andThen` b

    Match o alts ->
      indent (r "case" <+> o <+> r "of") (block alts)

    If alts ->
      indent (r "if") (block alts)

    Module b ->
      indent (r "closed") (p "(" <> b <> p ")")

    Constant c ->
      pp c

instance Pretty n => Pretty (Constant n Doc) where
  pp = \case
    Lam [Alt pats b] ->
      (p "\\" <> fields pats <+> p "->")
        `indent` b

    Lam clauses ->
      indent (p "\\") $ block $ map (\(Alt pats b) -> (p "|" <+> fields pats <+> p "->") `indent` b) clauses

    Object decls ->
      indent (p "{") (block decls) `andThen` p "}"

    Tuple es -> p "(" <> (fsep (punctuate (p ",") es)) <> p ")"
    List  es -> p "[" <> (fsep (punctuate (p ",") es)) <> p "]"

    Literal lit -> pp lit

instance Pretty (Literal Doc) where
  pp = \case
    Integer i -> g (text i)
    Float   f -> g (text f)
    String s [] -> g (quotes (text s))
    String s list -> ppSI (g ("\"" <> text s <> "#{")) list
      where
        ppSI s []           =       s
        ppSI s [(b, e)]     = ppSI (s <+> b <+> g ("}#" <> text e <> "\"")) []
        ppSI s ((b, e) : r) = ppSI (s <+> b <+> g ("}#" <> text e <> "#{")) r

instance Pretty n => Pretty (Alt n Doc) where
  pp (Alt pats b) =
    (block (map ((p "|" <+>) . pp) pats) <+> p "->")
      `indent` b

instance Pretty n => Pretty (Decl n Doc) where
  pp = \case
    Capture n -> r "pin" <+> h (pp n)
    Func n pats b (Where []) ->
      list (h (pp n)) pats <+> p "=" `indent` b

    Func n pats b (Where ws) ->
      (list (h (pp n)) pats <+> p "=" `indent` b)
        `indent` (r "where" `indent` block ws)

    Bind pat b (Where []) ->
      pp pat `indent` b

    Bind pat b (Where ws) ->
      (pp pat `indent` b)
        `indent` (r "where" `indent` block ws)

    Data d -> pp d

    Import c (Just pat) ->
      (r "import" <+> chain c) `indent` pp pat

    Import c Nothing ->
      r "import" <+> chain c

instance Pretty n => Pretty (Data n) where
  pp = \case
    AddType name ctors ->
      (r "data" <+> h (pp name)) `indent` block ctors

instance Pretty n => Pretty (Ctor n) where
  pp = \case
    Ctor name fs ->
      (p "|" <+> h (pp name)) `indent` fields fs
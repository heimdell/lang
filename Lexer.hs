
module Lexer where

import qualified Producer
import qualified Stream
import qualified Position
import qualified Category

import Control.Monad.State.Strict
import Control.Monad.Writer.Strict
import Control.Monad.Except

import qualified Data.Set as Set
import qualified Data.Map as Map

import Prelude hiding (lex)

import Debug.Trace

type T a = Map.Map Char (Category.T a)
type M   = StateT Stream.T (Except Failed)

data Failed = Failed String Position.T
  deriving Show

data Lexeme a
  = Lexeme a
  | InvalidChar Char
  deriving Show

lex :: T a -> M [(Position.T, Lexeme a)]
lex lexer = do
  s <- get
  case Stream.raw s of
    c : rest -> do
      p <- Producer.anyError Producer.position
      res <- case Map.lookup c lexer of
        Just cat -> do
          a <- Producer.mapError
            (Failed (Category.name cat) p)
            (Category.producer cat)
          return (Lexeme a)

        Nothing -> do
          c <- Producer.anyError Producer.next
          return (InvalidChar c)

      s <- lex lexer
      return ((p, res) : s)

    [] -> do
      return []

dropInvalidChars :: [(Position.T, Lexeme a)] -> [(Position.T, a)]
dropInvalidChars list =
  [ (pos, a)
  | (pos, Lexeme a) <- list
  ]

run :: M a -> String -> Either Failed a
run action str = runExcept (evalStateT action $ Stream.from str)

runOnFile :: M a -> String -> ExceptT Failed IO a
runOnFile action fname = do
  text <- lift $ readFile fname
  either throwError return $ run action text

empty :: T a
empty = Map.empty

add :: Category.T a -> T a -> T a
add cat
  = Map.unionWith Category.orElse
  $ Map.fromList
  $ zip (Category.chars cat)
  $ repeat cat

-- | Put additional "{;}" based on the layout.
insertBraces
  :: (Eq a, Show a)
  => (a -> Bool)  -- if token initiates layout level
  -> (a -> Bool)  -- if token is ignored
  -> (a, a, a)    -- "{" ";" "}"
  -> [(Position.T, a)]
  -> [(Position.T, a)]
insertBraces
    initiator
    ignored
    (open, sep, close)
    list
  =
      execWriter
    $ evalStateT (mapM click list *> dropEnoughStack (Position.Pos 1 1) 0)
    $ ([-2], False)
  where
    click (pos, a) = do
      unless (ignored a) do
        dropEnoughStack pos (Position.col pos)

        get >>= \case
          (top : stack, next) -> do

            when next do
              tell [(pos, open)]
              put (Position.col pos : top : stack, False)

            when (Position.col pos == top) do
              tell [(pos, sep)]

            when (initiator a) do
              (s, _) <- get
              put (s, True)

      tell [(pos, a)]

    dropEnoughStack pos level = do
      get >>= \case
        (top : stack, bool) -> do
          when (top > level && not bool) do
            tell [(pos, close)]
            put (stack, bool)
            dropEnoughStack pos level



module Position where

data T = Pos
  { line :: Int
  , col  :: Int
  }
  | Eof
  deriving (Show, Eq, Ord)

advance :: Char -> T -> T
advance '\n' (Pos line _)   = Pos (line + 1) 1
advance  _   (Pos line col) = Pos line (col + 1)
advance  _    Eof           = error "advance: Eof"

advanceString :: String -> T -> T
advanceString = flip $ foldl $ flip advance

start :: T
start = Pos 1 1

here :: [(Position.T, i)] -> Position.T
here ((p, _) : _) = p
here   _          = Position.Eof

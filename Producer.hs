
module Producer
  ( M
  , run
  , mapError
  , anyError
  , next
  , position
  , punctuation
  , whileIs
  , structure
  , nameLike
  )
  where

import qualified Position
import qualified Stream

import Control.Applicative
import Control.Arrow
import Control.Monad.State.Strict
import Control.Monad.Except

import qualified Data.Set as Set
import Data.Foldable

type M = StateT Stream.T (Except ())

run :: M a -> Stream.T -> Either () (a, Stream.T)
run action = runExcept . runStateT action

mapError :: e -> M a -> StateT Stream.T (Except e) a
mapError e = mapStateT $ mapExcept (const e +++ id)

anyError :: M a -> StateT Stream.T (Except e) a
anyError = mapError $ error "should not happen"

position :: M Position.T
position = gets Stream.position

lookahead :: M Char
lookahead = do
  s <- get
  guard (not (Stream.ended s))
  gets Stream.current

skip :: M ()
skip = do
  modify Stream.advance

next :: M Char
next = do
  c <- lookahead
  skip
  return c

satisfying :: (Char -> Bool) -> M Char
satisfying pred = do
  c <- next
  guard (pred c)
  return c

is :: String -> Char -> Bool
is = flip Set.member . Set.fromList

whileIs :: String -> M String
whileIs = many . satisfying . is

token :: String -> M String
token str = do
  for_ str (satisfying . (==))
  return str

anyTokenOf :: [String] -> M String
anyTokenOf = foldr1 (<|>) . map token

takeCharsUntil :: M a -> M (String, a)
takeCharsUntil stop = do
    st <- stop
    return ([], st)
  `catchError` \() -> do
    c <- escaped
    (s, st) <- takeCharsUntil stop
    return (c : s, st)

escaped :: M Char
escaped = do
    token "\\"
    next
  <|>
    next

structure :: ([String], [String]) -> M (String, String, String)
structure (opens, closes) = do
  open <- anyTokenOf opens
  (body, close) <- takeCharsUntil (anyTokenOf closes)
  return (open, body, close)

punctuation :: String -> M String
punctuation = (pure <$>) . satisfying . is

nameLike :: String -> String -> M String
nameLike h t = (:) <$> satisfying (is h) <*> whileIs t
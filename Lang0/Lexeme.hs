
module Lang0.Lexeme where

import Control.Lens

import Pretty
import Util

import Prelude hiding ((<>))

{-
  The type for token.
-}
data Lexeme
  = StringPart Bool Bool String
  | Number String
  | Name String
  | Dot
  | Lambda
  | Loop
  | LBrace
  | RBrace
  | LBracket
  | RBracket
  | LParen
  | RParen
  | Separator
  | Spaces String
  | Comment String
  | Documentation String
  deriving (Eq)

makePrisms ''Lexeme

{-
  Non-colored "toString"
-}
instance Show Lexeme where
  show = \case
    StringPart op clo body -> (op ? "}#" $ "\"") ++ body ++ (clo ? "#{" $ "\"")
    Number s               -> s
    Name   s               -> s
    Dot                    -> "."
    Lambda                 -> "\\"
    Loop                   -> "@"
    LBrace                 -> "{"
    RBrace                 -> "}"
    LBracket               -> "["
    RBracket               -> "]"
    LParen                 -> "("
    RParen                 -> ")"
    Separator              -> ";"
    Spaces s               -> s
    Comment s              -> "(*" ++ s ++ "*)"
    Documentation s        -> "(**" ++ s ++ "**)"

{-
  Colored "toString"
-}
instance Pretty Lexeme where
  pp = \case
    StringPart op clo body -> tint (Light Green) $ (if op then "}#" else "\"") <> text body <> (if clo then "#{" else "\"")
    Number s               -> tint (Light Blue) $ text s
    Name   s               -> text s
    Dot                    -> tint (Light White) $ "."
    Lambda                 -> tint (Light White) $ "\\"
    Loop                   -> tint (Light White) $ "@"
    LBrace                 -> tint (Light White) $ "{"
    RBrace                 -> tint (Light White) $ "}"
    LBracket               -> tint (Light White) $ "["
    RBracket               -> tint (Light White) $ "]"
    LParen                 -> tint (Light White) $ "("
    RParen                 -> tint (Light White) $ ")"
    Separator              -> tint (Light White) $ ";"
    Spaces s               -> text s
    Comment s              -> tint (Light Black) $ "(*" <> text s <> "*)"
    Documentation s        -> tint (Light Yellow) $ "(**" <> text s <> "**)"



let
  import sdata.list  as { use sort, use reverse }
  import sdata.set   as set
  import sdata.map   as map
  import sdata.maybe as maybe

  a-star estimate neighbors distance eps start =
    let
      initial-queue = map.singleton (cost start 0) ([start], 0)

      cost point traversed = + (estimate point) traversed

      go visited queue =
        maybe.then (map.minView) \((Cons point _, distance), rest-queue) ->
          if
            | < (estimate point) eps ->
              maybe.return path

            | else ->
              let
                near = list.filter
                  (\pt -> set.notMember pt visited)
                  (neighbors point)

                batch = list.for near \pt ->
                  let
                    distance1 = + distance (disp pt point)
                  ( cost point distance1
                  , ( : pt path
                    , distance1
                    )
                  )

              go (set.add point visited) (map.inserts batch rest-queue)

    maybe.map reverse (go set.empty initial-queue)

object
  use a-star
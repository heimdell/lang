
module Stream where

import qualified Position

data T = Stream
  { position :: Position.T
  , raw      :: String
  }
  deriving Show

advance :: T -> T
advance (Stream pos (c : s)) = Stream (Position.advance c pos) s

ended :: T -> Bool
ended = null . raw

current :: T -> Char
current = head . raw

from :: String -> T
from = Stream Position.start

(**
    This is a list module
**)

{
  data T =
    | Cons head tail
    | Nil

  (*
      We're using the @-loop here
  *)
  foldr zero op @go =
      \
      | (Cons x xs) -> op x (go xs)
      | Nil         -> zero
    where
      foo = bar

  (**
    The map function
  **)
  map f = foldr Nil (Cons . f)

  test = case foo bar of
    | 1 -> foo
    | _ -> bar

  interpolation-test descr =
    "hello \#{ #{ descr }# \"world\""
}

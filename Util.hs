
module Util where

(<|) :: a -> (a -> b) -> b
(<|) = flip ($)

infix 1 ?
(?) :: Bool -> a -> a -> a
(bool ? yes) no = if bool then yes else no
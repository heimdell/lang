
module Parser where

import Control.Lens

import Control.Applicative
import Control.Monad.State.Strict
import Control.Monad.Except
import Control.Monad.Identity

import Data.List

import qualified Position
import Pretty hiding ((<>))

import Prelude

newtype T i o = Parser
  { unParser
      :: ExceptT Error
      (  State [(Position.T, i)]
      )  o
  }
  deriving newtype
    ( Functor
    , Applicative
    , Monad
    , MonadState [(Position.T, i)]
    )

newtype Error
  = Expected [String]
  deriving stock (Show)
  deriving newtype (Semigroup, Monoid)

instance Pretty Error where
  pp (Expected []) = "Something unexpected"
  pp (Expected [s]) = "Expected" <+> text s
  pp (Expected list) = "Expected" <+> go (map text $ nub list)
    where
      go [s, p]  = s <+> "or" <+> p
      go (s : r) = (s <> ",") <+> go r

instance MonadError Error (T i) where
  throwError = Parser . throwError
  catchError ma ema
    = Parser $ ExceptT $ do
      s  <- get
      a  <- runExceptT (unParser ma)
      s' <- get
      case a of
        Left e
          | here s == here s' -> do
            b <- runExceptT (unParser (ema e))
            s'' <- get
            case b of
              Left e'
                | here s' == here s'' ->
                  return $ Left $ e <> e'

              other -> do
                return other
        other -> do
          return other

expected :: String -> T i o
expected msg = do
  throwError (Expected [msg])

try :: T i o -> T i o
try action = Parser $ ExceptT do
  s   <- get
  res <- runExceptT $ unParser action
  case res of
    Left e -> do
      put s
      return (Left e)

    other -> do
      return other

instance Alternative (T i) where
  empty = do
    throwError $ Expected ["something"]

  l <|> r = l `catchError` \_ -> r

here = Position.here

run :: T i a -> [(Position.T, i)] -> (Either Error a, [(Position.T, i)])
run (Parser p) input = runState (runExceptT p) input

match :: Fold i a -> T i a
match prizm = do
  s <- get
  case s^? _head._2.prizm of
    Just a -> do
      modify tail
      return a
    Nothing -> do
      throwError (Expected [])

infix 1 <?>
(<?>) :: T i o -> [String] -> T i o
p <?> msgs = decorate msgs p

eof :: T i ()
eof = do
  s <- get
  unless (here s == Position.Eof) do
    throwError $ Expected ["end of file"]

pos :: T i Position.T
pos = gets here

select :: [T i a] -> T i a
select = foldr1 (<|>)

addErr :: [String] -> [String] -> Maybe Position.T -> Maybe Position.T -> [String]
addErr e es (Just new) (Just old) =
  case compare new old of
    LT -> es
    EQ -> e ++ es
    GT -> e
addErr _ es Nothing  (Just _) = es
addErr e _  (Just _) Nothing  = e
addErr e es _        _        = e ++ es

movePos :: Maybe Position.T -> Maybe Position.T -> Maybe Position.T
movePos (Just new) (Just old) = Just $ max new old
movePos  a          b         = a <|> b

being :: Eq a => a -> Prism' a ()
being a = prism (\() -> a) (\a' -> if a == a' then Right () else Left a')

decorate :: [String] -> T i o -> T i o
decorate msgs action =
  try action `catchError` \(Expected _) ->
    throwError (Expected msgs)
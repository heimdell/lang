
module Reconstruct where

import Control.Lens
import Control.Monad.State.Strict
import Control.Monad.Writer.Strict

import qualified Position
import qualified Lexer
import Pretty
import Util

-- | Build printable colored representation of AST.
reconstruct :: forall a. (Pretty a, Show a) => [(Position.T, a)] -> String
reconstruct list
  = show
  $ execWriter
  $ flip evalStateT (getLineStart (Position.here list))
  $ mapM_ findPlace
  $ list
  where
    findPlace :: (Position.T, a) -> StateT (Int, Int) (Writer Doc) ()
    findPlace (pos, a) = do
      (l, c) <- get
      times (Position.line pos - l) do
        tell (text "\n")

      times
          (Position.line pos - l > 0 ?
            Position.col pos
            $ Position.col pos - c)
        do
          tell space

      tell (pp a)

      let end = Position.advanceString (show a) pos

      put (Position.line end, Position.col end)

    getLineStart Position.Eof = (1, 1)
    getLineStart (Position.Pos l c) = (l, 1)

times n _ | n <= 0 = return ()
times n x = x >> times (n - 1) x

module Pretty (module Pretty, module PP) where

import Text.PrettyPrint as PP

import Prelude hiding ((<>))

class Pretty p where
  pp :: p -> Doc

data Color
  = Black
  | Red
  | Green
  | Yellow
  | Blue
  | Magenta
  | Cyan
  | White
  deriving (Eq, Ord, Enum)

data Contrast a
  = Dark a
  | Light a

toCode :: Contrast Color -> String
toCode (Dark  c) = show $ fromEnum c + 30
toCode (Light c) = show (fromEnum c + 30) ++ ";1"

tint :: Contrast Color -> Doc -> Doc
tint c doc = zeroWidthText ("\ESC[" ++ toCode c ++ "m") <> doc <> zeroWidthText ("\ESC[0m")

module Category where

import Control.Applicative

import qualified Producer

data T a = Category
  { name     :: String
  , chars    :: String
  , producer :: Producer.M a
  }

from :: String -> String -> Producer.M a -> (a -> b) -> T b
from name chars producer f = Category name chars (f <$> producer)

punctuation :: String -> String -> (String -> a) -> T a
punctuation name chars ctor =
  Category name chars (ctor <$> Producer.punctuation chars)

punctuation' :: String -> String -> a -> T a
punctuation' name chars ctor =
  Category name chars (const ctor <$> Producer.punctuation chars)

whileIs :: String -> String -> (String -> a) -> T a
whileIs name chars ctor =
  Category name chars (ctor <$> Producer.whileIs chars)

whileIs' :: String -> String -> a -> T a
whileIs' name chars ctor =
  Category name chars (const ctor <$> Producer.whileIs chars)

structure :: String -> ([String], [String]) -> ((String, String, String) -> a) -> T a
structure name (opens, closes) ctor =
  Category name (map head opens) (ctor <$> Producer.structure (opens, closes))

nameLike :: String -> String -> String -> (String -> a) -> T a
nameLike name start rest ctor =
  Category name start (ctor <$> Producer.nameLike start rest)

Category a b c `orElse` Category d e f =
  Category (a ++ " or " ++ b) b (c <|> f)
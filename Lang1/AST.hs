
module Lang1.AST where

import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.State.Strict

import Data.Fix

import qualified Position

data Name = Name String Position.T
  deriving stock (Eq)

type Expr = Fix Expr_

data Expr_ self
  = Expr Position.T (Expr__ self)

data Expr__ self
  = Var        Name
  | App      { f :: self
             , x :: self
             }
  | Let      { decls :: [(Name, Maybe self)], body :: self }
  | View     { obj   :: self
             , prism :: self
             , yes   :: self
             , no    :: self
             }
  | Object     [(Name, self)]
  | Ctor       Int
  | Literal    Literal
  deriving stock (Functor, Foldable, Traversable)

data Literal
  = Integer    Integer
  | Float      Double
  | String     String

newtype Env = Env { getEnv :: [(Name, Value)] }

data Value
  = BIF       (Value -> M)
  | Constant   Literal
  | Struct   { nameHash :: Int, arity :: Int, args :: [Value] }
  | Closure    Env

data Error
  = NotDefined   Name   Position.T
  | CannotApply         Position.T
  | NotAPrism           Position.T
  | NotAnInteger        Position.T
  | NotAString          Position.T
  | NotAFloat           Position.T
  | NotAnObject         Position.T
  | HasNo        Name   Position.T
  | MatchFailed         Position.T
  | CannotMatchFunction Position.T

type M  = ExceptT                Error  (ReaderT Env (StateT Int IO)) Value
type M_ = ExceptT (Position.T -> Error) (ReaderT Env (StateT Int IO)) Value

cataExpr :: (Expr__ M_ -> M_) -> (Expr_ M -> M)
cataExpr f whole@(Fix (Expr pos expr_)) = withExceptT ($ pos) $ f whole

eval :: Expr -> M
eval = cata $ cataExpr \case
  Var name -> do
    Env env <- ask
    case lookup name env of
      Just it -> return it
      Nothing -> throwError (NotDefined name)

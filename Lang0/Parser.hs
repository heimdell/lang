
module Lang0.Parser where

import Data.Maybe
import Data.List (intercalate)
import Data.Fix
import Data.Composition
import Control.Applicative
import Control.Arrow ((***), (+++))
import Control.Lens
import Control.Monad
import Control.Monad.State.Strict

import Parser
import qualified Lang0.Lexeme as I
import qualified Lang0.AST as O
import qualified Position
import qualified Reconstruct
import Pretty

import qualified Lang0.Lexer as Lexer0
import qualified Lexer

import Debug.Trace

data Name = Name [String] Position.T
  deriving (Eq, Ord)

mergeNames :: [Name] -> Name
mergeNames [n] = n
mergeNames (Name n p : Name m _ : r) = mergeNames (Name (n ++ m) p : r)

instance Show Name where
  show (Name s _) = intercalate "." s

instance Pretty Name where
  pp (Name s _) = sep $ punctuate (tint (Light White) ".") $ map text s

type I = I.Lexeme
type O = O.AST Name

parser :: Parser.T I O
parser = toplevel

toplevel :: Parser.T I O
toplevel = do
  letPrefices <- many letPrefix
  body <- app
  return (foldl (flip (Fix .: O.Let)) body letPrefices)

app :: Parser.T I O
app = do
  terms <- some term
  return (foldl1 (Fix .: O.App) terms)

term :: Parser.T I O
term = select
  [ group
  , var
  , ctor
  , case_
  , if_
  , module_
  , (Fix . O.Constant) <$> constant
  ]

constant :: Parser.T I (O.Constant Name O)
constant = select
  [ lam
  , object
  , tuple
  , list
  , O.Literal <$> literal
  ]

literal :: Parser.T I (O.Literal O)
literal = select [float_, integer_, stringInterpolation]

integer_ :: Parser.T I (O.Literal O)
integer_ = do
  i <- match I._Number <?> ["integer"]
  return $ O.Integer $ i

float_ :: Parser.T I (O.Literal O)
float_ = do
  i <- try do
    i <- match I._Number <?> ["float"]
    match I._Dot <?> ["."]
    return i
  r <- match I._Number <?> ["decimals"]
  return $ O.Float $ i ++ "." ++ r

stringInterpolation :: Parser.T I (O.Literal O)
stringInterpolation = do
  (b, clo) <- match _Start <?> ["start of string literal"]
  if clo
  then do
    r <- go
    return $ O.String b r
  else do
    return $ O.String b []
  where
    go :: Parser.T I [(O, String)]
    go = do
      v <- toplevel
      (s, clo) <- match _Rest <?> ["}#-piece of string literal"]
      if clo
      then do
        r <- go
        return ((v, s) : r)
      else do
        return [(v, s)]

_Rest :: Fold I (String, Bool)
_Rest = I._StringPart . folding \(op, clo, b) ->
  if op then Just (b, clo) else Nothing

_Start :: Fold I (String, Bool)
_Start = I._StringPart . folding \(op, clo, b) ->
  if not op then Just (b, clo) else Nothing

lam :: Parser.T I (O.Constant Name O)
lam = do
  try (match I._Lambda <?> ["lambda (\\)"]) <|> reserved "fun"
  try $ optional $ match (I._Name.being "|") <?> ["|"]
  az <- nakedAlt `sepBy` (match (I._Name.being "|") <?> ["|"])
  return $ O.Lam az

p `sepBy` s = do
    x  <- p
    xs <- many (s *> p)
    return (x : xs)
  <|>
    return []

block :: Show a => Parser.T I a -> Parser.T I [a]
block =
  wrapped
    ( match I._LBrace    <?> ["{"]
    , match I._Separator <?> [";", "newline"]
    , match I._RBrace    <?> ["}"]
    )

object :: Parser.T I (O.Constant Name O)
object = do
  optional $ try $ reserved "object"
  ds <- block decl
  return $ O.Object ds

list :: Parser.T I (O.Constant Name O)
list = do
  match I._LBracket <?> ["list"]
  ds <- toplevel `sepBy` (match I._Separator <?> [";"])
  match I._RBracket <?> ["]"]
  return $ O.List ds

tuple :: Parser.T I (O.Constant Name O)
tuple = do
  ds <- wrapped1
    ( match I._LParen    <?> ["tuple"]
    , match I._Separator <?> ["semicolon"]
    , match I._RParen    <?> [")"]
    )
    toplevel
  return $ O.Tuple ds

module_ :: Parser.T I O
module_ = do
  reserved "closed"
  expr <- term
  return $ Fix $ O.Module expr

case_ :: Parser.T I O
case_ = do
  reserved "case"
  b <- toplevel
  reserved "of"
  alts <- block alt
  return $ Fix $ O.Match b alts

alt :: Parser.T I (O.Alt Name O)
alt = do
  reserved "|"
  nakedAlt

nakedAlt :: Parser.T I (O.Alt Name O)
nakedAlt = do
  ps <- try pat `sepBy` reserved "|"
  reserved "->"
  b <- toplevel
  return (O.Alt ps b)

if_ :: Parser.T I O
if_ = do
  reserved "if"
  alts <- block ifAlt
  return (Fix $ O.If alts)

ifAlt :: Parser.T I (O.IfAlt O)
ifAlt = do
  cs <- some do
    reserved "|"
    toplevel
  reserved "->"
  b <- toplevel
  return $ O.IfAlt cs b

group :: Parser.T I O
group = try do
  match I._LParen <?> ["expression in parenthes"]
  expr <- toplevel
  match I._RParen <?> [")"]
  return expr

var :: Parser.T I O
var = do
  n <- name
  return $ Fix $ O.Var n

ctor :: Parser.T I O
ctor = do
  n <- ctorName
  return $ Fix $ O.Var n

letPrefix :: Parser.T I [O.Decl Name O]
letPrefix = do
  reserved "let"
  decls <- block decl
  return decls

decl :: Parser.T I (O.Decl Name O)
decl = do
  func <|> bind <|> data_ <|> import_ <|> capture

capture :: Parser.T I (O.Decl Name O)
capture = do
  reserved "use"
  n <- name
  return $ O.Capture n

func :: Parser.T I (O.Decl Name O)
func = do
  n  <- name <?> ["function name"]
  ps <- many patTerm
  reserved "="
  b  <- toplevel
  ws <- optional $ try do
    reserved "where"
    block decl

  return $ O.Func n ps b (O.Where $ fromMaybe [] ws)

bind :: Parser.T I (O.Decl Name O)
bind = do
  reserved "match"
  p <- pat
  reserved "="
  b <- toplevel
  ws <- optional do
    reserved "where"
    block decl

  return $ O.Bind p b (O.Where $ fromMaybe [] ws)

data_ :: Parser.T I (O.Decl Name O)
data_ = do
  reserved "data"
  n <- ctorName
  reserved "of"
  cs <- block ctorDef
  return $ O.Data (O.AddType n cs)

pat :: Parser.T I (O.Pattern Name O)
pat = do
  names <- many rename
  p <- select
    [ isCtor
    , viewPat
    , patTerm
    ]
  return $ foldl O.As p names

patTerm :: Parser.T I (O.Pattern Name O)
patTerm = do
  select
    [ patGroup
    , isTuple
    , isList
    , isObject
    , isLiteral
    , strictPat
    , loop
    , hole
    ]

patGroup :: Parser.T I (O.Pattern Name O)
patGroup = try do
  match I._LParen <?> ["pattern in parenthes"]
  p <- pat
  match I._RParen <?> [")"]
  return p

strictPat :: Parser.T I (O.Pattern Name O)
strictPat = do
  reserved "!"
  p <- pat
  return $ O.Strict p

isDecl :: T I (O.IsDecl Name O)
isDecl = select
  [ isRealDecl
  , isCapture
  ]

isRealDecl :: T I (O.IsDecl Name O)
isRealDecl = do
  n <- name
  reserved "="
  p <- pat
  return $ O.IsDecl n p

isCapture :: T I (O.IsDecl Name O)
isCapture = do
  reserved "use"
  n <- name
  return $ O.IsCapture n

hole :: Parser.T I (O.Pattern Name O)
hole = do
  n <- name
  return $ O.Hole n

loop :: Parser.T I (O.Pattern Name O)
loop = do
  match I._Loop <?> ["@loop pattern"]
  n <- name
  return $ O.Loop n

viewPat :: Parser.T I (O.Pattern Name O)
viewPat = do
  f <- try do
    f <- toplevel
    reserved "->"
    return f
  p <- pat
  return $ O.View f p

isTuple :: Parser.T I (O.Pattern Name O)
isTuple = do
  p <- try do
    match I._LParen    <?> ["tuple pattern"]
    p <- pat
    match I._Separator <?> ["semicolon"]
    return p
  ps <- pat `sepBy` (match I._Separator <?> ["semicolon"])
  match I._RParen <?> [")"]
  return $ O.IsTuple (p : ps)

isList :: Parser.T I (O.Pattern Name O)
isList = do
  ps <- wrapped
    ( match I._LBracket  <?> ["list pattern"]
    , match I._Separator <?> ["semicolon"]
    , match I._RBracket  <?> ["]"]
    )
    pat
  return $ O.IsList ps

isObject :: Parser.T I (O.Pattern Name O)
isObject = do
  optional $ try $ reserved "object"
  ps <- wrapped
    ( match I._LBrace    <?> ["object pattern"]
    , match I._Separator <?> ["semicolon"]
    , match I._RBrace    <?> ["}"]
    )
    isDecl

  return $ O.IsObject ps

wrapped :: Show a => (Parser.T I (), Parser.T I (), Parser.T I ()) -> Parser.T I a -> Parser.T I [a]
wrapped (open, sep, close) p = do
  open
  go1 (close >> return [])
  where
    go1 exit = go exit <|> exit

    go exit = do
      a <- p
      az <- (sep >> go1 exit) <|> exit
      return (a : az)


wrapped1 :: (Parser.T I (), Parser.T I (), Parser.T I ()) -> Parser.T I a -> Parser.T I [a]
wrapped1 (open, sep, close) p = do
  a <- try do
    open
    a <- p
    sep
    return a
  az <- go1 (close >> return [])
  return (a : az)
  where
    go1 exit = go exit <|> exit

    go exit = do
      a <- p
      az <- (sep >> go1 exit) <|> exit
      return (a : az)

isCtor :: Parser.T I (O.Pattern Name O)
isCtor = do
  n <- ctorName
  ps <- many patTerm
  return (O.IsCtor n ps)

isLiteral :: Parser.T I (O.Pattern Name O)
isLiteral = do
  l <- literal
  return $ O.IsLiteral l

rename :: Parser.T I Name
rename = try do
  n <- name
  reserved "as"
  return n

ctorDef :: Parser.T I (O.Ctor Name)
ctorDef = do
  reserved "|"
  n  <- ctorName
  fs <- many name
  return $ O.Ctor n fs

import_ :: Parser.T I (O.Decl Name O)
import_ = do
  reserved "import"
  c <- name
  p <- optional do
    reserved "as"
    pat

  return $ O.Import [c] p

name :: Parser.T I Name
name = do
  q <- qualificator
  n <- rawName
  return $ mergeNames $ q ++ [n]

ctorName :: Parser.T I Name
ctorName = do
  q <- qualificator
  n <- rawCtorName
  return $ mergeNames $ q ++ [n]

qualificator :: Parser.T I [Name]
qualificator = do
  many do
    try do
      n <- rawName
      match I._Dot <?> ["."]
      return n

rawName :: Parser.T I Name
rawName = try do
  p <- pos
  s <- get
  it <- match I._Name <?> ["name"]

  unless (head it `notElem` ['A'.. 'Z'] ++ ":") do
    put s
    expected "non-constructor"

  unless (it `notElem` reservedWords) do
    put s
    expected "non-reserved"

  return $ Name [it] p

reserved :: String -> Parser.T I ()
reserved w = do
  unless (w `elem` reservedWords) do
    error $ "`" ++ w ++ "` is not a reserved word"

  match (I._Name.being w) <?> ["`" ++ w ++ "`"]

rawCtorName :: Parser.T I Name
rawCtorName = try $ do
  p  <- pos
  it <- match I._Name <?> ["constructor name"]
  unless (head it `elem` ['A'.. 'Z'] ++ ":") do
    expected "constructor"
  unless (it `notElem` reservedWords) do
    expected "non-reserved"
  return $ Name [it] p

reservedWords :: [String]
reservedWords = words
  "let in case of = if | module -> => data where as import use ! @ use val match object closed fun"

test = do
  text  <- readFile "lexer-test.ml"
  let input = Lexer0.run text
  case input of
    Left err -> putStrLn (show err)
    Right a  -> do
      putStrLn $ Reconstruct.reconstruct a
      let (ast, rest) = Parser.run parser a
      print $ either pp pp $ ast
      putStrLn "at"
      putStrLn (Reconstruct.reconstruct rest)

module Lang0.Lexer where

import Control.Arrow
import Control.Monad.Except

import qualified Lexer
import qualified Reconstruct
import qualified Category
import qualified Position
import Util

import Lang0.Lexeme

lexer :: Lexer.T Lexeme
lexer = Lexer.empty
  <| Lexer.add (Category.punctuation' "dot"           "."  Dot)
  <| Lexer.add (Category.nameLike     "name" alphaOp alphaOpNum Name)

  <| Lexer.add (Category.whileIs      "number" num         Number)
  <| Lexer.add (Category.punctuation' "lambda"        "\\" Lambda)
  <| Lexer.add (Category.punctuation' "loop"          "@"  Loop)
  <| Lexer.add (Category.punctuation' "left brace"    "{"  LBrace)
  <| Lexer.add (Category.punctuation' "right brace"   "}"  RBrace)
  <| Lexer.add (Category.punctuation' "left bracket"  "["  LBracket)
  <| Lexer.add (Category.punctuation' "right bracket" "]"  RBracket)
  <| Lexer.add (Category.punctuation' "left paren"    "("  LParen)
  <| Lexer.add (Category.punctuation' "right paren"   ")"  RParen)
  <| Lexer.add (Category.punctuation' "separator"     ",;" Separator)
  <| Lexer.add (Category.whileIs      "spaces"        " \n\t\r" Spaces)

  -- The string with interpolation like that: "hello, {$ fucking $} world!"
  <| Lexer.add
    ( Category.structure
        "string"
        ( ["\"", "}#"]
        , ["\"", "#{"]
        )
        ( \(open, body, close) ->
          StringPart (open == "}#") (close == "#{") body
        )
    )

  <| Lexer.add
    ( Category.structure
        "comment"
        (["(*"], ["*)"])
        (\(_, body, _) -> Comment body)
    )

  <| Lexer.add
    ( Category.structure
        "documentation"
        (["(**"], ["**)"])
        (\(_, body, _) -> Documentation body)
    )
  where
    alphaOp = ['a'.. 'z'] ++ ['A'.. 'Z'] ++ "_!~#$%^&*-+=|/?><"
    num     = ['0'.. '9']
    alphaOpNum = alphaOp ++ num

run :: String -> Either Lexer.Failed [(Position.T, Lexeme)]
run text
  = fmap (Lexer.insertBraces
      (`elem`
        [ Name "let"
        , Name "object"
        , Name "of"
        , Name "where"
        , Name "if"
        ])
      (\case
        Spaces        {} -> True
        Comment       {} -> True
        Separator        -> True
        _                -> False
      )
      (LBrace, Separator, RBrace))
  $ (id +++ (filter (not . meh . snd) . Lexer.dropInvalidChars))
  $ Lexer.run (Lexer.lex lexer)
  $ text
  where
    meh = \case
      Spaces  {} -> True
      Comment {} -> True
      Documentation {} -> True
      _          -> False

-- test :: IO ()
-- test = do
--   mblexemes <- runExceptT $ Lexer.runOnFile (Lexer.lex lexer) "lexer-test.ml"
--   case mblexemes of
--     Right lexemes -> do
--       let
--         purified = Lexer.dropInvalidChars lexemes
--         inserted = Lexer.insertBraces
--           (`elem`
--             [ Name "let"
--             , Name "function"
--             , Name "of"
--             , Name "where"
--             , Name "object"
--             ])
--           (\case
--             Spaces        {} -> True
--             Comment       {} -> True
--             -- Documentation {} -> True
--             Separator        -> True
--             _                -> False
--           )
--           (LBrace, Separator, RBrace)
--           purified

--       putStrLn $ Reconstruct.reconstruct inserted

--     Left err -> do
--       error (show err)